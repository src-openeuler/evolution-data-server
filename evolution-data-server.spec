%define glib2_version 2.68
%define gtk3_version 3.20
%define gtk4_version 4.4
%define gtk_doc_version 1.9
%define goa_version 3.8
%define libsecret_version 0.5
%define libgdata_version 0.15.1
%define libgweather_version 4.0.0
%define libical_version 3.0.7
%define libsoup_version 3.1.1
%define nss_version 3.14
%define sqlite_version 3.7.17
%define webkit2gtk_version 2.34.0
%define webkit2gtk4_version 2.36.0
%define json_glib_version 1.0.4

%define credential_modules_dir %{_libdir}/evolution-data-server/credential-modules
%define camel_provider_dir %{_libdir}/evolution-data-server/camel-providers
%define ebook_backends_dir %{_libdir}/evolution-data-server/addressbook-backends
%define ecal_backends_dir %{_libdir}/evolution-data-server/calendar-backends
%define modules_dir %{_libdir}/evolution-data-server/registry-modules
%define uimodules_dir %{_libdir}/evolution-data-server/ui-modules

%global dbus_service_name_address_book	org.gnome.evolution.dataserver.AddressBook10
%global dbus_service_name_calendar	org.gnome.evolution.dataserver.Calendar8
%global dbus_service_name_sources	org.gnome.evolution.dataserver.Sources5
%global dbus_service_name_user_prompter	org.gnome.evolution.dataserver.UserPrompter0

%if "%{?_eds_dbus_services_prefix}" != ""
%global dbus_service_name_address_book	%{?_eds_dbus_services_prefix}.%{dbus_service_name_address_book}
%global dbus_service_name_calendar	%{?_eds_dbus_services_prefix}.%{dbus_service_name_calendar}
%global dbus_service_name_sources	%{?_eds_dbus_services_prefix}.%{dbus_service_name_sources}
%global dbus_service_name_user_prompter	%{?_eds_dbus_services_prefix}.%{dbus_service_name_user_prompter}
%endif

%{!?with_docs: %global with_docs 1}

Name:           evolution-data-server
Version:        3.46.2
Release:        6
Summary:        Backend data server for Evolution
License:        LGPL-2.1-or-later
URL:            https://wiki.gnome.org/Apps/Evolution
Source:         https://download.gnome.org/sources/%{name}/3.46/%{name}-%{version}.tar.xz
Patch6001:      backport-evolution-data-server-use-webkitgtk-6.0-api.patch
Patch6002:      backport-evolution-data-server-try-harder-to-support-webkitgtk-6.0.patch
Patch6003:      backport-evolution-data-server-Update-for-removal-of-WebKitGTK-sandbox-API.patch
Patch6004:      backport-evolution-data-server-OAuth2-use-WebKitNetworkSession-to-manage-proxy.patch
Patch6005:      backport-evolution-data-server-icu76.patch
Patch6006:      backport-evolution-data-server-cmake-3.15.patch

Provides:       evolution-webcal = %{version}
Obsoletes:      evolution-webcal < 2.24.0 compat-evolution-data-server310-libcamel < 3.12

Recommends:     pinentry-gui
Requires:       %{name}-langpacks = %{version}-%{release}


BuildRequires: cmake make
BuildRequires: gcc
BuildRequires: gcc-c++
BuildRequires: gettext
BuildRequires: gperf
BuildRequires: gtk-doc >= %{gtk_doc_version}
BuildRequires: ninja-build
BuildRequires: vala
BuildRequires: systemd
BuildRequires: sendmail

BuildRequires: pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gio-unix-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gmodule-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(icu-i18n)
BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(goa-1.0) >= %{goa_version}
BuildRequires: pkgconfig(gweather4) >= %{libgweather_version}
BuildRequires: pkgconfig(libical-glib) >= %{libical_version}
BuildRequires: pkgconfig(libsecret-unstable) >= %{libsecret_version}
BuildRequires: pkgconfig(libsoup-3.0) >= %{libsoup_version}
BuildRequires: pkgconfig(libxml-2.0)
BuildRequires: pkgconfig(nspr)
BuildRequires: pkgconfig(nss) >= %{nss_version}
BuildRequires: pkgconfig(sqlite3) >= %{sqlite_version}
BuildRequires: pkgconfig(webkit2gtk-4.1) >= %{webkit2gtk_version}
BuildRequires: pkgconfig(webkitgtk-6.0) >= %{webkit2gtk4_version}
BuildRequires: pkgconfig(json-glib-1.0) >= %{json_glib_version}
BuildRequires: pkgconfig(libcanberra-gtk3)
BuildRequires: openldap-devel >= 2.0.11
BuildRequires: krb5-devel >= 1.11

#BuildRequires: libphonenumber-devel
#BuildRequires: protobuf-devel
#BuildRequires: boost-devel
#BuildRequires: abseil-cpp-devel

BuildRequires:  chrpath

%description
The evolution-data-server package provides a personal information management application that provides integrated
mail, calendaring and address book functionality. The evolution-data-server package provides a single database for
common, desktop-wide information, such as a user's address book or calendar events.

%package        devel
Summary:        Tests case and libraries for evolution-data-server development
Requires: %{name}%{?_isa} = %{version}-%{release}

Requires: pkgconfig(goa-1.0) >= %{goa_version}
Requires: pkgconfig(gweather4) >= %{libgweather_version}
Requires: pkgconfig(libical-glib) >= %{libical_version}
Requires: pkgconfig(libsecret-unstable) >= %{libsecret_version}
Requires: pkgconfig(libsoup-3.0) >= %{libsoup_version}
Requires: pkgconfig(sqlite3) >= %{sqlite_version}
Requires: pkgconfig(webkit2gtk-4.1) >= %{webkit2gtk_version}
Requires: pkgconfig(webkitgtk-6.0) >= %{webkit2gtk4_version}
Requires: pkgconfig(json-glib-1.0) >= %{json_glib_version}

Provides:       evolution-data-server-tests
Obsoletes:      evolution-data-server-tests < %{version}-%{release}

%description    devel
This package contains the tests case and libraries needed for the evolution-data-server library.

%package        langpacks
Summary:        Translations for evolution-data-server
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description    langpacks
This package contains translations for evolution-data-server.

%package        perl
Summary:        Supplemental utilities that require Perl
Requires:       %{name} = %{version}-%{release}
Requires:       perl-interpreter

%description    perl
This package contains supplemental utilities for evolution-data-server that require Perl.

%package_help

%prep
%autosetup -p1

%build
if ! pkg-config --exists nss; then
  echo "Unable to find suitable version of nss to use!"
  exit 1
fi

export CPPFLAGS="-I%{_includedir}/et"
export CFLAGS="$RPM_OPT_FLAGS -DLDAP_DEPRECATED -fPIC -I%{_includedir}/et -Wno-deprecated-declarations"

%cmake -G Ninja -DENABLE_MAINTAINER_MODE=OFF -DWITH_LIBDB=OFF \
        -DENABLE_FILE_LOCKING=fcntl -DENABLE_DOT_LOCKING=OFF -DENABLE_INTROSPECTION=ON \
        -DWITH_SYSTEMDUSERUNITDIR=%{_userunitdir} \
        %if "%{?_eds_dbus_services_prefix}" != ""
        -DDBUS_SERVICES_PREFIX=%{?_eds_dbus_services_prefix} \
        %endif
        -DENABLE_VALA_BINDINGS=ON -DENABLE_INSTALLED_TESTS=ON -DWITH_OPENLDAP=ON -DWITH_KRB5=ON \
        -DENABLE_SMIME=ON -DENABLE_LARGEFILE=ON -DENABLE_SMIME=ON -DENABLE_GTK_DOC=ON -DWITH_PHONENUMBER=OFF

sed -i "302 d" %{__cmake_builddir}/docs/reference/evolution-data-server/evolution-data-server-docs.sgml
%cmake_build -j1

%install
%cmake_install


mkdir $RPM_BUILD_ROOT/%{uimodules_dir} || :
mkdir $RPM_BUILD_ROOT/%{credential_modules_dir} || :

find %{buildroot} -name '*.so.*' -exec chmod +x {} \;

%find_lang %{name}

chrpath -d %{buildroot}/%{_libdir}/%{name}/calendar-backends/*.so
chrpath -d %{buildroot}/%{_libdir}/%{name}/camel-providers/*.so
chrpath -d %{buildroot}/%{_libdir}/%{name}/credential-modules/*.so
chrpath -d %{buildroot}/%{_libdir}/%{name}/registry-modules/*.so
chrpath -d %{buildroot}/%{_libdir}/%{name}/addressbook-backends/*.so

chrpath -d %{buildroot}%{_libdir}/*.so*
chrpath -d %{buildroot}%{_libexecdir}/camel-index-control-1.2
chrpath -d %{buildroot}%{_libexecdir}/evolution-addressbook-factory
chrpath -d %{buildroot}%{_libexecdir}/evolution-addressbook-factory-subprocess
chrpath -d %{buildroot}%{_libexecdir}/evolution-calendar-factory-subprocess
chrpath -d %{buildroot}%{_libexecdir}/evolution-user-prompter
chrpath -d %{buildroot}%{_libexecdir}/evolution-calendar-factory
chrpath -d %{buildroot}%{_libexecdir}/camel-index-control-1.2
chrpath -d %{buildroot}%{_libexecdir}/evolution-source-registry
chrpath -d %{buildroot}%{_libexecdir}/evolution-scan-gconf-tree-xml

chrpath -d %{buildroot}%{_libexecdir}/%{name}/evolution-alarm-notify
chrpath -d %{buildroot}%{_libexecdir}/%{name}/list-sources
chrpath -d %{buildroot}%{_libexecdir}/%{name}/addressbook-export
file `find %{buildroot}%{_libexecdir}/%{name}/installed-tests -type f` | grep -w ELF | awk -F: '{print $1}' | xargs chrpath -d

mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/%{name}" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%license COPYING
%doc README ChangeLog NEWS
%{_libdir}/libcamel-1.2.so.64
%{_libdir}/libcamel-1.2.so.64.0.0
%{_libdir}/libebackend-1.2.so.11
%{_libdir}/libebackend-1.2.so.11.0.0
%{_libdir}/libebook-1.2.so.21
%{_libdir}/libebook-1.2.so.21.1.3
%{_libdir}/libebook-contacts-1.2.so.4
%{_libdir}/libebook-contacts-1.2.so.4.0.0
%{_libdir}/libecal-2.0.so.2
%{_libdir}/libecal-2.0.so.2.0.0
%{_libdir}/libedata-book-1.2.so.27
%{_libdir}/libedata-book-1.2.so.27.0.0
%{_libdir}/libedata-cal-2.0.so.2
%{_libdir}/libedata-cal-2.0.so.2.0.0
%{_libdir}/libedataserver-1.2.so.27
%{_libdir}/libedataserver-1.2.so.27.0.0
%{_libdir}/libedataserverui-1.2.so.4
%{_libdir}/libedataserverui-1.2.so.4.0.0
%{_libdir}/libedataserverui4-1.0.so.0
%{_libdir}/libedataserverui4-1.0.so.0.0.0

%{_libdir}/girepository-1.0/Camel-1.2.typelib
%{_libdir}/girepository-1.0/EBackend-1.2.typelib
%{_libdir}/girepository-1.0/EBook-1.2.typelib
%{_libdir}/girepository-1.0/EBookContacts-1.2.typelib
%{_libdir}/girepository-1.0/ECal-2.0.typelib
%{_libdir}/girepository-1.0/EDataBook-1.2.typelib
%{_libdir}/girepository-1.0/EDataCal-2.0.typelib
%{_libdir}/girepository-1.0/EDataServer-1.2.typelib
%{_libdir}/girepository-1.0/EDataServerUI-1.2.typelib
%{_libdir}/girepository-1.0/EDataServerUI4-1.0.typelib

%{_libexecdir}/camel-gpg-photo-saver
%{_libexecdir}/camel-index-control-1.2
%{_libexecdir}/camel-lock-helper-1.2
%{_libexecdir}/evolution-addressbook-factory
%{_libexecdir}/evolution-addressbook-factory-subprocess
%{_libexecdir}/evolution-calendar-factory
%{_libexecdir}/evolution-calendar-factory-subprocess
%{_libexecdir}/evolution-scan-gconf-tree-xml
%{_libexecdir}/evolution-source-registry
%{_libexecdir}/evolution-user-prompter

%dir %{_libexecdir}/evolution-data-server
%{_libexecdir}/evolution-data-server/addressbook-export
%{_libexecdir}/evolution-data-server/evolution-alarm-notify
%{_libexecdir}/evolution-data-server/list-sources

%{_sysconfdir}/xdg/autostart/org.gnome.Evolution-alarm-notify.desktop
%{_datadir}/applications/org.gnome.Evolution-alarm-notify.desktop

# GSettings schemas:
%{_datadir}/GConf/gsettings/evolution-data-server.convert
%{_datadir}/glib-2.0/schemas/org.gnome.Evolution.DefaultSources.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution-data-server.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution-data-server.addressbook.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution-data-server.calendar.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.eds-shell.gschema.xml
%{_datadir}/glib-2.0/schemas/org.gnome.evolution.shell.network-config.gschema.xml

%{_datadir}/evolution-data-server
%{_datadir}/dbus-1/services/%{dbus_service_name_address_book}.service
%{_datadir}/dbus-1/services/%{dbus_service_name_calendar}.service
%{_datadir}/dbus-1/services/%{dbus_service_name_sources}.service
%{_datadir}/dbus-1/services/%{dbus_service_name_user_prompter}.service
%{_datadir}/pixmaps/evolution-data-server

%{_userunitdir}/evolution-addressbook-factory.service
%{_userunitdir}/evolution-calendar-factory.service
%{_userunitdir}/evolution-source-registry.service
%{_userunitdir}/evolution-user-prompter.service

%dir %{_libdir}/evolution-data-server
%dir %{credential_modules_dir}
%dir %{camel_provider_dir}
%dir %{ebook_backends_dir}
%dir %{ecal_backends_dir}
%dir %{modules_dir}
%dir %{uimodules_dir}

%{_libdir}/evolution-data-server/libedbus-private.so

# Camel providers:
%{camel_provider_dir}/libcamelimapx.so
%{camel_provider_dir}/libcamelimapx.urls

%{camel_provider_dir}/libcamellocal.so
%{camel_provider_dir}/libcamellocal.urls

%{camel_provider_dir}/libcamelnntp.so
%{camel_provider_dir}/libcamelnntp.urls

%{camel_provider_dir}/libcamelpop3.so
%{camel_provider_dir}/libcamelpop3.urls

%{camel_provider_dir}/libcamelsendmail.so
%{camel_provider_dir}/libcamelsendmail.urls

%{camel_provider_dir}/libcamelsmtp.so
%{camel_provider_dir}/libcamelsmtp.urls

# e-d-s extensions:
%{credential_modules_dir}/module-credentials-goa.so
%{ebook_backends_dir}/libebookbackendcarddav.so
%{ebook_backends_dir}/libebookbackendfile.so
%{ebook_backends_dir}/libebookbackendldap.so
%{ecal_backends_dir}/libecalbackendcaldav.so
%{ecal_backends_dir}/libecalbackendcontacts.so
%{ecal_backends_dir}/libecalbackendfile.so
%{ecal_backends_dir}/libecalbackendgtasks.so
%{ecal_backends_dir}/libecalbackendhttp.so
%{ecal_backends_dir}/libecalbackendweather.so
%{ecal_backends_dir}/libecalbackendwebdavnotes.so
%{modules_dir}/module-cache-reaper.so
%{modules_dir}/module-google-backend.so
%{modules_dir}/module-gnome-online-accounts.so
%{modules_dir}/module-oauth2-services.so
%{modules_dir}/module-outlook-backend.so
%{modules_dir}/module-secret-monitor.so
%{modules_dir}/module-trust-prompt.so
%{modules_dir}/module-webdav-backend.so
%{modules_dir}/module-yahoo-backend.so

%config(noreplace) /etc/ld.so.conf.d/*

%files devel
%{_includedir}/evolution-data-server
%{_libdir}/libcamel-1.2.so
%{_libdir}/libebackend-1.2.so
%{_libdir}/libebook-1.2.so
%{_libdir}/libebook-contacts-1.2.so
%{_libdir}/libecal-2.0.so
%{_libdir}/libedata-book-1.2.so
%{_libdir}/libedata-cal-2.0.so
%{_libdir}/libedataserver-1.2.so
%{_libdir}/libedataserverui-1.2.so
%{_libdir}/libedataserverui4-1.0.so
%{_libdir}/pkgconfig/camel-1.2.pc
%{_libdir}/pkgconfig/evolution-data-server-1.2.pc
%{_libdir}/pkgconfig/libebackend-1.2.pc
%{_libdir}/pkgconfig/libebook-1.2.pc
%{_libdir}/pkgconfig/libebook-contacts-1.2.pc
%{_libdir}/pkgconfig/libecal-2.0.pc
%{_libdir}/pkgconfig/libedata-book-1.2.pc
%{_libdir}/pkgconfig/libedata-cal-2.0.pc
%{_libdir}/pkgconfig/libedataserver-1.2.pc
%{_libdir}/pkgconfig/libedataserverui-1.2.pc
%{_libdir}/pkgconfig/libedataserverui4-1.0.pc
%{_datadir}/gir-1.0/Camel-1.2.gir
%{_datadir}/gir-1.0/EBackend-1.2.gir
%{_datadir}/gir-1.0/EBook-1.2.gir
%{_datadir}/gir-1.0/EBookContacts-1.2.gir
%{_datadir}/gir-1.0/ECal-2.0.gir
%{_datadir}/gir-1.0/EDataBook-1.2.gir
%{_datadir}/gir-1.0/EDataCal-2.0.gir
%{_datadir}/gir-1.0/EDataServer-1.2.gir
%{_datadir}/gir-1.0/EDataServerUI-1.2.gir
%{_datadir}/gir-1.0/EDataServerUI4-1.0.gir
%{_datadir}/vala/vapi/camel-1.2.deps
%{_datadir}/vala/vapi/camel-1.2.vapi
%{_datadir}/vala/vapi/libebackend-1.2.deps
%{_datadir}/vala/vapi/libebackend-1.2.vapi
%{_datadir}/vala/vapi/libebook-1.2.deps
%{_datadir}/vala/vapi/libebook-1.2.vapi
%{_datadir}/vala/vapi/libebook-contacts-1.2.deps
%{_datadir}/vala/vapi/libebook-contacts-1.2.vapi
%{_datadir}/vala/vapi/libecal-2.0.deps
%{_datadir}/vala/vapi/libecal-2.0.vapi
%{_datadir}/vala/vapi/libedata-book-1.2.deps
%{_datadir}/vala/vapi/libedata-book-1.2.vapi
%{_datadir}/vala/vapi/libedata-cal-2.0.deps
%{_datadir}/vala/vapi/libedata-cal-2.0.vapi
%{_datadir}/vala/vapi/libedataserver-1.2.deps
%{_datadir}/vala/vapi/libedataserver-1.2.vapi
%{_datadir}/vala/vapi/libedataserverui-1.2.deps
%{_datadir}/vala/vapi/libedataserverui-1.2.vapi
%{_datadir}/vala/vapi/libedataserverui4-1.0.deps
%{_datadir}/vala/vapi/libedataserverui4-1.0.vapi
%{_libdir}/libetestserverutils.so
%{_libdir}/libetestserverutils.so.0
%{_libdir}/libetestserverutils.so.0.0.0
%{_libexecdir}/%{name}/installed-tests
%{_datadir}/installed-tests

%files langpacks -f %{name}.lang

%if %{with_docs}
%files help
%{_datadir}/gtk-doc/html/*
%endif

%files perl
%{_libexecdir}/evolution-data-server/csv2vcard

%changelog
* Sun Mar 09 2025 Funda Wang <fundawang@yeah.net> - 3.46.2-6
- build with icu 76
- fix build with cmake 4.0

* Sun Feb 23 2025 Funda Wang <fundawang@yeah.net> - 3.46.2-5
- use webkitgtk-6.0 api

* Tue Feb 18 2025 Funda Wang <fundawang@yeah.net> - 3.46.2-4
- adopt to new cmake macro

* Wed Nov 20 2024 Funda Wang <fundawang@yeah.net> - 3.46.2-3
- adopt to new cmake macro

* Sat Mar 04 2023 wangkai <wangkai385@h-partners.com> - 3.46.2-2
- Remove rpath in installed-tests

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 3.46.2-1
- Update to 3.46.2

* Wed Aug 24 2022 caodongxia <caodongxia@h-partners.com> -3.44.3-4
- Remove rpath

* Fri Jul 22 2022 caodongxia <caodongxia@h-partners.com> - 3.44.3-3
- Remove unless find-lang.sh during posttrans

* Wed Jul 20 2022 liyanan <liyanan32@h-partners.com> - 3.44.3-2
- Modify to single-threaded compilation to solve occasional compilation failures 

* Mon Jul 18 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.44.3-1
- Update to 3.44.3

* Thu Jun 16 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.44.2-3
- replace to libsoup3-devel

* Wed Jun 15 2022 wenlong ding <wenlong.ding@turbolinux.com.cn> 3.44.2-2
- Change libgweather version to 4.0.0

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 3.44.2-1
- Update to 3.44.2

* Mon Jun 7 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-2
- Update Release, Requires, to solve this rmp package depends on itsself when install

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.4-1
- Upgrade to 3.38.4
- Update Version, Release, Source, BuildRequires, Requires, Obsoletes
- Update stage 'install', use sed delete one line that will cause fault

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-3
- delete redundant files

* Sat Sep 21 2019 dongjian <dongjian13@huawei.com> - 3.30.1-2
- Package init
